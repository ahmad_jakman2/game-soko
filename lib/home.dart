import 'package:flutter/material.dart';
import 'package:soko/player.dart';
import 'package:soko/utils/play_type.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
         ElevatedButton(onPressed: (){
          Navigator.of(context).push(MaterialPageRoute(builder: (c){
            return MyHomePage(playType: PlayType.User, level: 1, title: '');
          }));
        }, child: Text('User Player')),
        SizedBox(height: 15,),
        ElevatedButton(onPressed: (){
          Navigator.of(context).push(MaterialPageRoute(builder: (c){
            return MyHomePage(playType: PlayType.DFS, level: 3, title: '');
          }));
        }, child: Text('DFS Player')),
        SizedBox(height: 15,),
        ElevatedButton(onPressed: (){
          Navigator.of(context).push(MaterialPageRoute(builder: (c){
            return MyHomePage(playType: PlayType.BFS, level: 3, title: '');
          }));
        }, child: Text('BFS Player')),
        SizedBox(height: 15,),
        ElevatedButton(onPressed: (){
          Navigator.of(context).push(MaterialPageRoute(builder: (c){
            return MyHomePage(playType: PlayType.UCS, level: 3, title: '');
          }));
        }, child: Text('UCS Player')),
        SizedBox(height: 15,),
        ElevatedButton(onPressed: (){
          Navigator.of(context).push(MaterialPageRoute(builder: (c){
            return MyHomePage(playType: PlayType.A_STAR, level: 5, title: '');
          }));
        }, child: Text('A_STAR Player'))
    ],);
  }
}