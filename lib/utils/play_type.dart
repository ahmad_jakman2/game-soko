enum PlayType {
  DFS,
  BFS,
  UCS,
  A_STAR,
  User
}