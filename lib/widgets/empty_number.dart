// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

class EmptyNumber extends StatefulWidget {
  int number;
  double fontSize;
  EmptyNumber({
    Key? key,
    required this.number,
    this.fontSize = 60,
  }) : super(key: key);

  @override
  State<EmptyNumber> createState() => _EmptyNumberState();
}

class _EmptyNumberState extends State<EmptyNumber> {
  @override
  Widget build(BuildContext context) {
    return Container(
       decoration: BoxDecoration(color: Colors.white,border: Border.all(color:Colors.grey,width:1)),
      child: Center(
        child: Text(
          widget.number.toString(),
          style: TextStyle(fontSize: widget.fontSize, color: Colors.grey),
        ),
      ),
    );
  }
}
