// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

class NumberCell extends StatefulWidget {
  int number;
  bool isMatch;
  double fontSize;
  NumberCell({
    Key? key,
    required this.isMatch,
    required this.number,
    this.fontSize = 60,
  }) : super(key: key);

  @override
  State<NumberCell> createState() => _NumberCellState();
}

class _NumberCellState extends State<NumberCell> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: widget.isMatch ? Colors.red  :Colors.white,
      child: Center(
        child: Text(
          widget.number.toString(),
          style: TextStyle(fontSize: widget.fontSize, color: widget.isMatch ?Colors.white : Colors.black),
        ),
      ),
    );
  }
}
