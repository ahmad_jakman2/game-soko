import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WallCell extends StatefulWidget {
  const WallCell({Key? key}) : super(key: key);

  @override
  State<WallCell> createState() => _WallCellState();
}

class _WallCellState extends State<WallCell> {
  @override
  Widget build(BuildContext context) {
    return Container (
       decoration: BoxDecoration(color: Color(0xFFFF77A8),border: Border.all(color:Colors.grey,width:1)),
    );
  }
}
