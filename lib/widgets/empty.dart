import 'package:flutter/material.dart';

class EmptyCell extends StatefulWidget {
  const EmptyCell({Key? key}) : super(key: key);

  @override
  State<EmptyCell> createState() => _EmptyCell();
}

class _EmptyCell extends State<EmptyCell> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Color(0xB1B2B7),border: Border.all(color:Colors.grey,width:1)),
      
    );
  }
}
