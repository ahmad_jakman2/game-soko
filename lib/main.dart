import 'dart:html';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:soko/widgets/empty.dart';
import 'package:soko/widgets/empty_number.dart';
import 'package:soko/widgets/number.dart';
import 'package:soko/widgets/wall.dart';
import 'package:soko/models/cell.dart';
import 'package:soko/player.dart';
import 'package:soko/utils/play_type.dart';

import 'home.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner:false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home:  Home(),
    );
  }
}


