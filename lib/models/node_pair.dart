// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:soko/models/node.dart';

class NodePair {
  int cost;
  Node node;
  NodePair({
    required this.cost,
    required this.node,
  });
}
