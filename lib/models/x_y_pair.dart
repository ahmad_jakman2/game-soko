// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:soko/models/cell.dart';

import 'node.dart';

class XYPair {
  int x;
  int y;
  XYPair({
    required this.x,
    required this.y,
  });
  
}
