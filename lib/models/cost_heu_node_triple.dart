// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:soko/models/node.dart';

class CHNodeTriple {
  int cost;
  int heuristic;
  Node node;
  CHNodeTriple({
    required this.cost,
    required this.heuristic,
    required this.node,
  });
  
}
